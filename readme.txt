Have no idea what base would work best for you? Check out this document! https://drive.google.com/open?id=1NYoHjmyNmERtXhjMqux2Sm3084MP88qHceoEOceykkA
Have questions, want more stage documentation, or want to join the project? Join our Discord! https://discord.gg/s7c8763

Permission is granted by the authors of this project to use these as bases for custom stages.
In fact, this is encouraged, as adding internal documentation to custom stages should make edits and further improvements easier!

The Annotated Stage Project aims to combine all known stage knowledge in an easy-to-read format to increase ease of stage modding in Brawl.
The project adds in MSBIN documentation for every "special" ModelData as well as other useful stage components.

There are currently two ends of the Annotated Stage Project, each with slightly different goals.

- vBrawl (AKA Vanilla or Unmodified Brawl): Focuses on providing clean, easy-to-use bases for completely custom stages. Annotations are focused on how to control and manipulate the gimmicks of the stage.
- Project M: Focuses on providing clean, easy-to-use bases primarily for 1:1s. Annotations are focused on how the stage was built and what elements need to be kept intact to make a 1:1 of the stage.